This is a cards-against-humanity webapp. To run, just edit settings.json to fit your server and port, and then run $python3 cards_against_decency.py 

The script is using the default bottle webserver, so if you have too many players at one time, it will have to execute each request sequentially, and it will fall over.

A live demo is at http://nimbulus.ml:8000

This demo is hosted on a *very* low end vps, so please be kind and don't kill it c:


This is still a very much work-in-progress. Priorities include integrating it into apache or some other proper webserver,f fixing the CSS on various pages, removing extraneous 
copyright notices, etc. 

