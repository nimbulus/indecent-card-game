from bottle import route, run, template, static_file, request, redirect, response, get, post
#from textblob import TextBlob
from threading import Thread
import time
import os
import random
import string
import json
import bottle
# Change working directory so relative paths (and template lookup) work again
#os.chdir(os.path.dirname(__file__))
# ... build or import your bottle application here ...
# Do NOT use bottle.run() with mod_wsgi
application = bottle.default_app()
"""
DONE: Implement uniform questions, individualized answers,
printing out which answer you submitted
webpage updates with responses from server
Made buttons clickable, filter client such that their own answers are not shown
made it read as UTF-8 when reading questions and answers
Fix bug where people can join the game even after its full
Make the answer page prettier (voting.html)
Fix logic such that once done, the page redirects back to game, and another round is started.
Add a waiting page for players to see who won each round
fix starting page so you can't enter non-integer number of players
Get host and port configuration from a config file or from the command line vs hard-coding

TODO:
Show other players, and their points on each page
Fix the CSS on voting.html so that the cells dont get squished/elongated off the screen
Fix bug where double clicking "join" results in 2 players being registered (local javascript to disable buttons?)
Document voteAjax
DESIRABLE:




"""



dir_path = os.getcwd()
activeGames = {'gameID':{'numberOfPlayers':4, 'playersJoined':1, 'players':{'username1':0,'username2':0}}}
# a dict of active games, with number of players, that gets popped out when games complete
#{gameID, {'numberOfPlayers':#, 'playersJoined':#, players:{'name':score,}, currentSubmissions:{'name':'submission','_subs':0,'_counter':[0,username]} votes:{'name':num}}, currentQuestion:'Question', roundID:0 }}
def generateQuestions(gameID,roundID):
    random.seed(gameID+str(roundID))# by using the same seed, everyone on the same game will see the same questions
    with open('questions.txt', 'r+', encoding="utf-8") as questions:
        questions = questions.read()
    questions = questions.split('\n')
    return questions[random.randint(0,len(questions)-1)]

def generateAnswers(gameID,roundID,username):
    random.seed(str(username)+str(roundID)+str(gameID))# by using a different seed based on username, we can get different answers per player
    with open('answers.txt', 'r+', encoding="utf-8") as answers:
        answers = answers.read()
    answers = answers.split('\n')
    sindex = random.randint(0,len(answers)-1)
    return answers[sindex:sindex+4]

def randID(size=4, chars=string.ascii_uppercase):
    return ''.join(random.choice(chars) for _ in range(size))

@route('/index.pl')
def index():
    with open('cards_against_start.html') as f:
        u = f.read()
    udict = {'timefetched':time.strftime("%c"),}
    return template(u, **udict)

@post('/index.pl')
def game_handle():
    gameAction = request.forms.get('join_make') #name of the form
    gameActionParams = request.forms.get('landingText')
    if gameAction == "joinGame":
        redirect("/"+gameActionParams.upper())
    if gameAction == "makeGame" and gameActionParams !="": #check gameActionParams exists
        gameID = randID()
        activeGames.update({gameID:{'numberOfPlayers':gameActionParams, 'playersJoined':0,'roundID':0,'players':{},'currentSubmissions':{'_subs':0,'_counter':[0,'NaN']},'votes':{}, }}) #gameActionParams will be an int of number of players
        redirect("/"+gameID)
    else:
        redirect("/index.pl")
@get('/<pageName>')
def handleLanding(pageName):
    if pageName in activeGames and activeGames.get(pageName).get('playersJoined') < int(activeGames.get(pageName).get('numberOfPlayers')):
        with open('cards_against_game_load.html') as f:
            u = f.read()

        udict = {'numPlayers':activeGames[pageName]['numberOfPlayers'],'gameID':pageName}
        return template(u, **udict)
    else:
        redirect("/index.pl")


@get('/<pageName>/game/')
def handleGame(pageName):
    if pageName in activeGames:
        activeGames[pageName]['votes'] = {} #clears it at the beginning of each round.
        username = request.cookies.get('playerCookie','=')
        roundID = request.cookies.get('roundID')
        print("game type of roundID: "+str(type(roundID)))
        question = generateQuestions(pageName,roundID)
        answers = generateAnswers(pageName,roundID,username)
        activeGames.get(pageName).update({'currentQuestion':question})
        with open('gameplay.html') as f:
            u = f.read()
        udict = {'roundID':roundID,'gameID':pageName, 'username':username, 'question':question, 'answers':answers}
        return template(u, **udict)
    else:
        redirect('/index.pl')
    #TODO: Generate questions that are uniform for all clients, and supply answers.


@post('/<pageName>/game/')
def handleGame(pageName):
    if pageName in activeGames:
        username = request.cookies.get('playerCookie','=')
        roundID = request.cookies.get('roundID')
        #print(roundID)
        submission = dict(request.forms)['answer']
        question = activeGames[pageName]['currentQuestion']
        activeGames[pageName]['currentSubmissions'][username] = submission
        activeGames[pageName]['currentSubmissions']['_subs'] = activeGames[pageName]['currentSubmissions']['_subs']+1 #increments submissions counter
        #activeGames[pageName]['currentSubmissions']['_counter'][1] = username
        #print(submission)
        #Set up subs so that it is counter[0]+1, and set counter[1] to be the username, that acts
        #as the key for the actual new submission.
        #print(submission)
        with open('voting.html') as f:
            u = f.read()
        udict = {'question':question,'roundID':roundID,'gameID':pageName,'numPlayers':activeGames[pageName]['numberOfPlayers'], 'username':username, 'submission':submission}
        return template(u, **udict)
    else:
        redirect('/index.pl')

@route('/<pageName>/ajax', method='POST')
def ajax(pageName):
    #print(dict(request.forms))
    if pageName in activeGames:
        if list(request.forms)[0]=='heartbeat':
            #print('heartbeat caught')
            if activeGames.get(pageName).get('playersJoined')==int(activeGames.get(pageName).get('numberOfPlayers')):

                return "done"
            else:
                return "waiting"
        player = request.forms # {"playerNameForm": "playerName"}
        counter = activeGames.get(pageName).get('playersJoined') #gets the number of players joined, and sets their points to 0.
        if activeGames.get(pageName).get('playersJoined')==int(activeGames.get(pageName).get('numberOfPlayers')): #all the players have joined
            playerCookie = request.cookies.get('playerCookie', '-')
            response.set_cookie("roundID", str(0))
            if playerCookie =='-':
                #print("trying to set cookie")
                response.set_cookie("playerCookie", player.get('playerNameForm'))
            return "done"
        if activeGames.get(pageName).get('playersJoined')>int(activeGames.get(pageName).get('numberOfPlayers')):
            return "Error - too many people!"

        else:
            #not all the playesr have joined yet
            tempDict = {player['playerNameForm']:0, **activeGames.get(pageName).setdefault('players',{})} #updates the dict with the player ID. PlayerNameForm is from the formsdict, where the name of the player is in the key "playerNameForm". See HTMl doc for deets.
            activeGames.get(pageName).get('players').update(tempDict)
            activeGames.get(pageName).update({'playersJoined':counter+1})
            playerCookie = request.cookies.get('playerCookie', '-')
            if playerCookie == '-':
                response.set_cookie('playerCookie', player.get('playerNameForm'))
                response.set_cookie("roundID", str(0))
            return "waiting" #+ str(len(activeGames.get(pageName).get('players'))) + " "+ activeGames.get(pageName).get('numberOfPlayers')

    else:
        return "error 404"


@route('/<pageName>/game/ajax', method='POST')
def voteAjax(pageName):
    #print(dict(request.forms))
    if pageName in activeGames:
        #print("heartbeat caught for "+pageName)
        if activeGames[pageName]['currentSubmissions']['_subs'] >= int(activeGames[pageName]['numberOfPlayers']):
            retstring = ""
            for key in activeGames[pageName]['currentSubmissions']:
                if (key != "_subs" and key != "_counter"):
                    retstring = retstring+key+"~"+activeGames[pageName]['currentSubmissions'][key]+"|"
            return "voteSubmitted|"+retstring
        else:
            retstring = ""
            for key in activeGames[pageName]['currentSubmissions']:
                if (key != "_subs" and key != "_counter"):
                    retstring = retstring+key+"~"+activeGames[pageName]['currentSubmissions'][key]+"|"
            return retstring

@route('/<pageName>/ajax', method='GET')
def ajaxtest(pageName):
    if pageName in activeGames:
        return str(activeGames)
    else:
        return "game not found"

@post('/<pageName>/game/voting')
def handleVote(pageName):
    if pageName in activeGames:
        numberOfSubmissions = 0
        for electee in activeGames[pageName]['votes']:
            numberOfSubmissions += activeGames[pageName]['votes'][electee]

        if numberOfSubmissions < int(activeGames.get(pageName).get('numberOfPlayers')):
            username = request.cookies.get('playerCookie', '=')
            roundID = request.cookies.get('roundID')
            print(f"Current roundID: {roundID}")  # Debug: Log the current roundID

            # Get the vote and ensure it's in the correct format
            vote = dict(request.forms).get('vote', '')
            if not vote:
                return "Error: No vote submitted."

            vote_parts = vote.split('|')
            if len(vote_parts) < 2:
                return "Error: Invalid vote format. Expected 'answer|username'."

            answer, voted_user = vote_parts[0], vote_parts[1]

            activeGames[pageName]['currentSubmissions']['_subs'] = 0  # Resets the submissions counter

            with open('voteresults.html') as f:
                u = f.read()

            udict = {
                'roundID': roundID,
                'gameID': pageName,
                'numPlayers': activeGames[pageName]['numberOfPlayers'],
                'username': username,
            }

            # Increment roundID and set the cookie with a path of '/'
            new_roundID = str(int(roundID) + 1)
            response.set_cookie("roundID", new_roundID, path="/")
            print(f"Updated roundID: {new_roundID}")  # Debug: Log the updated roundID

            try:
                # Update votes for the voted user
                numVotes = activeGames[pageName]['votes'].get(voted_user, 0)
                activeGames[pageName]['votes'][voted_user] = numVotes + 1
                activeGames[pageName]['players'][voted_user] = activeGames[pageName]['players'].get(voted_user, 0) + 1
            except KeyError as e:
                return f"Error: {e}. Failed to update votes."

            return template(u, **udict)
        else:
            return str(activeGames[pageName]['votes'])
    else:
        redirect('/index.pl')

@route('/<pageName>/voting/ajax', method='POST')
def voteAjax(pageName):
    #print(dict(request.forms))
    if pageName in activeGames:
        #print("heartbeat caught for "+pageName)
        numberOfSubmissions=0
        for electee in activeGames[pageName]['votes']:
            numberOfSubmissions=numberOfSubmissions+activeGames[pageName]['votes'][electee]
        if numberOfSubmissions >= int(activeGames.get(pageName).get('numberOfPlayers')): #all votes submitted
            retstring = "done|"

            for electee in activeGames[pageName]['votes']:
                #print(str(activeGames[pageName]['votes'][electee]))
                retstring = retstring+electee+"~"+str(activeGames[pageName]['votes'][electee])+"|"

            return retstring
        else:#not all votes submitted
            return "waiting"


@route('/')
def root():
    redirect('/index.pl')
@route('/static/css/<filename>')
def server_static(filename):
    return static_file(filename, root=dir_path+'/static/css')
@route('/static/js/<filename>')
def server_static(filename):
    return static_file(filename, root=dir_path+'/static/js')



with open("settings.json") as f:
    settings=f.read()
    settings = json.loads(settings)
run(host=settings['server'], port=settings['port'])
